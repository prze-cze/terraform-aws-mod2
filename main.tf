terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.51.0"
      configuration_aliases = [ aws.virtual-machine ]
    }
  }
}


data "aws_subnet" "aws_snet" {
  id = var.subnet_id
}

data "aws_vpc" "aws_vpc_z2" {
  id = data.aws_subnet.aws_snet.vpc_id
}

data "aws_internet_gateway" "pub_ip_gw" {
  filter {
    name   = "attachment.vpc-id"
    values = [data.aws_subnet.aws_snet.vpc_id]
  }  
}


resource "aws_security_group" "allow_ssh_http" {
  name        = "${var.vm_name}-sg"
  description = "SSH 22, HTTP 80 port opened"
  vpc_id      = data.aws_vpc.aws_vpc_z2.id
  ingress {
    description = "SSH-22"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SSH-80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}



resource "aws_network_interface" "vm_inet" {
  subnet_id       = data.aws_subnet.aws_snet.id
  security_groups = [aws_security_group.allow_ssh_http.id]

  tags = {
    Name = "${var.vm_name}-inet"
  }
}

resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

locals {
  ssh_priv = tls_private_key.key.private_key_openssh
}

resource "aws_key_pair" "aws_key" {
  key_name   = "${var.vm_name}-ssh-key"
  public_key = tls_private_key.key.public_key_openssh
}


resource "aws_instance" "vm" {
  provider                    = aws.virtual-machine
  ami                         = var.source_image
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.aws_key.key_name
  user_data    = "${file("${path.module}/user-data.sh")}"
  user_data_replace_on_change = true

  network_interface {
    network_interface_id = aws_network_interface.vm_inet.id
    device_index = 0
  }

  tags = {
    Name = "${var.vm_name}-vm"
  }
}


