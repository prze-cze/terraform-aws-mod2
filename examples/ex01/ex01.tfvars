adresacja = "10.200.104.0/22"

subnets_pub_cidrs = {
  "10.200.104.0/28" = "eu-central-1a",
}

subnets_cidrs = {
  "10.200.105.0/28" = "eu-central-1a",
  "10.200.106.0/28" = "eu-central-1a",
  "10.200.107.0/28" = "eu-central-1a",
}

vm_name = "test-instance-123"

# source_image = "ami-0991068fb5349de54" ## fedora-coreos-37.20221106.3.0-x86_64
# source_image = "ami-0b4c74d41ee4bed78" ## CentOS-7-2111-20220825_1.x86_64-d9a3032a-921c-4c6d-b150-bde168105e42
source_image = "ami-0a261c0e5f51090b1" ## Amazon Linux 2 Kernel 5.10 AMI 2.0.20221210.1 x86_64 HVM gp2
# instance_type = "t2.micro"

