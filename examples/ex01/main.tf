terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.51.0"
      configuration_aliases = [
        aws.aws,
        aws.virtual-machine,
      ]
    }
  }
}


provider "aws" {
  region = "eu-central-1"
}

provider "aws" {
  alias  = "virtual-machine"
  region = "eu-central-1"
}


## Part 1 - preparation

variable "lokalizacja" {
  default = "eu-central-1"
}

variable "adresacja" {
  default = "10.200.100.0/22"
}

variable "subnets_pub_cidrs" {
  type = map(any)
  default = {
    "10.200.100.0/24" = "eu-central-1a",
  }
}

variable "subnets_cidrs" {
  type = map(any)
  default = {
    "10.200.101.0/24" = "eu-central-1a"
  }
}


module "aws1" {
  source = "git::https://gitlab.com/prze-cze/terraform-aws-mod1?ref=v0.1.1"

  adresacja         = var.adresacja
  subnets_cidrs     = var.subnets_cidrs
  subnets_pub_cidrs = var.subnets_pub_cidrs
  lokalizacja       = var.lokalizacja

}


## Part 2

variable "vm_name" {
  type = string
}

variable "source_image" {
  type    = string
  default = "ami-0a261c0e5f51090b1" ## Amazon Linux 2 Kernel 5.10 AMI 2.0.20221210.1 x86_64 HVM gp2
}

variable "instance_type" {
  type    = string
  default = "t2.micro"

}

module "aws2" {
  source = "git::https://gitlab.com/prze-cze/terraform-aws-mod2?ref=v0.1.1"

  providers = {
    aws                 = aws
    aws.virtual-machine = aws.virtual-machine
  }

  lokalizacja   = var.lokalizacja
  subnet_id     = module.aws1.subnets_ids[0]
  subnet_pub_id = module.aws1.subnets_pub_ids[0]
  vm_name       = "${var.vm_name}-vm1"
  source_image  = var.source_image
  instance_type = var.instance_type

}
module "aws3" {
  source = "git::https://gitlab.com/prze-cze/terraform-aws-mod2?ref=v0.1.1"

  providers = {
    aws                 = aws
    aws.virtual-machine = aws.virtual-machine
  }

  lokalizacja   = var.lokalizacja
  subnet_id     = module.aws1.subnets_pub_ids[0]
  subnet_pub_id = module.aws1.subnets_pub_ids[0]
  vm_name       = "${var.vm_name}-vm2"
  source_image  = var.source_image
  instance_type = var.instance_type

}

output "vm_info" {
  value = {
    vm_priv = module.aws2[*]
    vm_pub  = module.aws3[*]
    }
}


