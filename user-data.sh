#!/bin/bash

sudo yum update -y
sudo yum install -y httpd.x86_64
sudo systemctl start httpd.service
sudo systemctl enable httpd.service
echo "Hello World from <strong>$(hostname -f)</strong>" > /var/www/html/index.html
echo "Script done on: $(hostname -f)"

echo "Current user is $(id)"

echo "Enabling SSH PasswordAuthentication"

OLD_OPT="PasswordAuthentication no"
NEW_OPT="PasswordAuthentication yes"

sudo grep "$NEW_OPT" /etc/ssh/sshd_config
sudo sed -i -e "s/${OLD_OPT}/${NEW_OPT}/g" /etc/ssh/sshd_config
sudo systemctl restart sshd
sudo grep $NEW_OPT /etc/ssh/sshd_config

echo 'ch@ngeMe!n0vv' | sudo passwd --stdin "ec2-user"
 