output "vm_id" {
  value       = aws_instance.vm.id
  description = "vm_details: ID"
}

output "pub_ip" {
  value       = aws_instance.vm.public_ip
  description = "vm_details: IP pub"
}

output "priv_ip" {
  value       = aws_instance.vm.private_ip
  description = "vm_details: IP priv"
}
