variable "lokalizacja" {
  default = "eu-central-1"
}

variable "pub_ip" {
  default = false
}

variable "subnet_id" {
  type = string
}

variable "subnet_pub_id" {
  type = string
  default = ""
}

variable "vm_name" {
  type = string
}

variable "source_image" {
  type = string
  default = "ami-0a261c0e5f51090b1" ## Amazon Linux 2 Kernel 5.10 AMI 2.0.20221210.1 x86_64 HVM gp2
}

variable "instance_type" {
  type = string
  default = "t2.micro"

}
